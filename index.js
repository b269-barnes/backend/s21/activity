let users = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"];

console.log("Original Array:")
console.log(users);

function addItem(item){
    users[users.length] = item;
}

addItem("John Cena");
console.log(users);



function getItemByIndex(index){
    return users[index];
}

console.log(getItemByIndex(2));



function deletedLastItem(){
   
    let deleted_item = users[users.length - 1]

    return deleted_item;
}

console.log(deletedLastItem());
console.log(users);


function updateItemByIndex(updated_name, index){
    users[index] = updated_name;
}

updateItemByIndex("MJF", 3);
console.log(users);


function deleteAllItems(){
    users = [];
}

deleteAllItems();
console.log(users);


function isUsersArrayEmpty(){
    if(users.length > 0){
        return false
    } else {
        return true
    }

}

console.log(isUsersArrayEmpty())


users = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"];


function getIndexByItem(item){
    for(let index = 0; index < users.length; index++){
      
        if(item === users[index]) {
            return index;
        }
    }
}

console.log(getIndexByItem("Kurt Angle"));